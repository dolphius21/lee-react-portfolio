import React from 'react';
import Banner from './components/Banner';
import About from './components/About';
import Projects from './components/Projects';
import Footer from './components/Footer';

function App() {
  return (
    <React.Fragment>
      <Banner />
      <About />
      <Projects />
      <Footer />
    </React.Fragment>
  );
}

export default App;
