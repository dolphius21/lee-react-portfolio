import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

function Projects() {
  return (
    <section id="projects-section">
      <Container>
        <h3 className="section-heading">Projects</h3>
        <Row>
          <Col md={6} lg={4}>
            <div className="project" id="project-1">
              <div className="project-info">
                <a
                  href="https://dolphius21.github.io/four-card-feature-section/"
                  target="_blank"
                  rel="noreferrer"
                >
                  Four Card Feature Section
                </a>
              </div>
            </div>
          </Col>
          <Col md={6} lg={4}>
            <div className="project" id="project-2">
              <div className="project-info">
                <a
                  href="https://dolphius21.github.io/fylo-dark-theme-landing-page/"
                  target="_blank"
                  rel="noreferrer"
                >
                  Fylo Landing Page
                </a>
              </div>
            </div>
          </Col>
          <Col md={6} lg={4}>
            <div className="project" id="project-3">
              <div className="project-info">
                <a
                  href="https://dolphius21.github.io/clipboard-landing-page/"
                  target="_blank"
                  rel="noreferrer"
                >
                  Clipboard Landing Page
                </a>
              </div>
            </div>
          </Col>
          <Col md={6} lg={4}>
            <div className="project" id="project-4">
              <div className="project-info">
                <a
                  href="https://dolphius21.github.io/FAQ-Accordion-Card/"
                  target="_blank"
                  rel="noreferrer"
                >
                  FAQ Accordion Card
                </a>
              </div>
            </div>
          </Col>
          <Col md={6} lg={4}>
            <div className="project" id="project-5">
              <div className="project-info">
                <a
                  href="https://dolphius21.github.io/to_do_list_app/"
                  target="_blank"
                  rel="noreferrer"
                >
                  Todolist App
                </a>
              </div>
            </div>
          </Col>
          <Col md={6} lg={4}>
            <div className="project" id="project-6">
              <div className="project-info">
                <a href="https://dolphius21.github.io/Beatmaker/" target="_blank" rel="noreferrer">
                  Beatmaker
                </a>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
}

export default Projects;
