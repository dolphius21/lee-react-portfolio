import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import profileImg from '../assets/profile-img.jpg';
import { FaPenNib } from 'react-icons/fa';
import { FaBezierCurve } from 'react-icons/fa';
import { FaCode } from 'react-icons/fa';

function Skills() {
  return (
    <section id="about-section">
      <Container>
        <img src={profileImg} alt="" className="profile-img" />
        <h3 className="section-heading">About me</h3>
        <p>
          Hello you can call me Rudolph, I'm currenty a Graphic Designer who aspires to become a Web
          Developer. So far I am a newbie when it comes to coding. That's why I join Zuitt to
          further my knowledge in web development.
        </p>
        <Row className="pd-2">
          <Col lg={4} className="skill">
            <FaPenNib className="icon" />
            <h4 className="skillsHeading">Graphic Illustration</h4>
            <p>I'm an expert when it comes to creating vector-based graphics.</p>
          </Col>
          <Col lg={4} className="skill">
            <FaBezierCurve className="icon" />
            <h4 className="skillsHeading">Motion Graphics</h4>
            <p>I animate graphics that can be used in your marketing ads.</p>
          </Col>
          <Col lg={4} className="skill">
            <FaCode className="icon" />
            <h4 className="skillsHeading">Web Development</h4>
            <p>I know HTML and CSS and a little bit of JavaScript.</p>
          </Col>
        </Row>
      </Container>
    </section>
  );
}

export default Skills;
