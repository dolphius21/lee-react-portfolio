import React from 'react';
import { Container, Form, Button } from 'react-bootstrap';

function Footer() {
  return (
    <footer id="footer">
      <Container>
        <h3 className="section-heading">Contact Me</h3>
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" />
          </Form.Group>
          <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Label>Message</Form.Label>
            <Form.Control as="textarea" rows={3} />
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </Container>
      <p>© John Rudolph Lee | Graphic Designer/Web Developer</p>
    </footer>
  );
}

export default Footer;
