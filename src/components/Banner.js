import React from 'react';

// Import the necessary components from react-bootstrap
import { Navbar, Nav, Container, Jumbotron } from 'react-bootstrap';

const Banner = () => {
  return (
    <Jumbotron className="banner">
      <Container>
        <Navbar className="customNav" expand="lg">
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              <Nav.Link href="#about">ABOUT ME</Nav.Link>
              <Nav.Link href="#projects">PROJECTS</Nav.Link>
              <Nav.Link href="#contact">CONTACT</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <div className="bannerContent">
          <h1 className="heading">Hello, I'm John Rudolph Lee</h1>

          <p className="banner-text">
            A graphic designer, aspiring web developer and a lifelong learner.
          </p>
        </div>
      </Container>
    </Jumbotron>
  );
};

export default Banner;
